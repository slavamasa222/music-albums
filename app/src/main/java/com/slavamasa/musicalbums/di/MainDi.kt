package com.slavamasa.musicalbums.di

import androidx.room.Room
import com.slavamasa.feature_main.data.data_source.Database
import com.slavamasa.feature_main.data.data_source.daos.AlbumDao
import com.slavamasa.feature_main.data.data_source.daos.SongDao
import com.slavamasa.feature_main.data.remote.TunesApi
import com.slavamasa.feature_main.data.repository.RepositoryImpl
import com.slavamasa.feature_main.domain.repository.Repository
import com.slavamasa.feature_main.domain.usecases.*
import com.slavamasa.feature_main.presentation.viewmodels.AlbumViewModel
import com.slavamasa.feature_main.presentation.viewmodels.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://itunes.apple.com/"

val dataModule = module {

    single<Database> {
        Room.databaseBuilder(get(), Database::class.java, "MusicDatabase.db").build()
    }

    single<AlbumDao> { get<Database>().albumDao }

    single<SongDao> { get<Database>().songDao }

    single<TunesApi> {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(TunesApi::class.java)
    }

    single<Repository> { RepositoryImpl(tunesApi = get(), albumDao = get(), songDao = get()) }
}

val domainModule = module {

    factory<GetRemoteAlbums> { GetRemoteAlbums(repository = get()) }

    factory<GetRemoteSongsByAlbumId> { GetRemoteSongsByAlbumId(repository = get()) }

    factory<GetAlbumsByName> { GetAlbumsByName(repository = get()) }

    factory<GetAllAlbums> { GetAllAlbums(repository = get()) }

    factory<InsertAllAlbums> { InsertAllAlbums(repository = get()) }

    factory<GetAlbumById> { GetAlbumById(repository = get()) }

    factory<InsertAllSongs> { InsertAllSongs(repository = get()) }

    factory<GetAllSongsByAlbumId> { GetAllSongsByAlbumId(repository = get()) }

    factory<MusicUseCases> {
        MusicUseCases(
            getRemoteAlbums = get(),
            getRemoteSongsByAlbumId = get(),
            getAlbumsByName = get(),
            getAllAlbums = get(),
            insertAllAlbums = get(),
            getAlbumById = get(),
            insertAllSongs = get(),
            getAllSongsByAlbumId = get()
        )
    }
}

val presentationModule = module {

    viewModel<MainViewModel> { MainViewModel(musicUseCases = get()) }

    viewModel<AlbumViewModel>{ AlbumViewModel(musicUseCases = get()) }
}