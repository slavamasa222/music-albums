package com.slavamasa.musicalbums.app

import android.app.Application
import com.slavamasa.musicalbums.di.dataModule
import com.slavamasa.musicalbums.di.domainModule
import com.slavamasa.musicalbums.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(dataModule, domainModule, presentationModule)
        }
    }

}