package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName

data class AlbumsResponseDto(

    @SerializedName("resultCount")
    val resultCount: Int,

    @SerializedName("results")
    val albums: List<AlbumDto>
)