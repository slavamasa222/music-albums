package com.slavamasa.feature_main.data.data_source

import androidx.room.Database
import androidx.room.RoomDatabase
import com.slavamasa.feature_main.data.data_source.daos.AlbumDao
import com.slavamasa.feature_main.data.data_source.daos.SongDao
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.models.Song

@Database(
    entities = [Album::class, Song::class],
    version = 1
)
abstract class Database : RoomDatabase() {

    abstract val albumDao: AlbumDao
    abstract val songDao: SongDao
}