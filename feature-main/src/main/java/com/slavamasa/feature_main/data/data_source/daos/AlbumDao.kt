package com.slavamasa.feature_main.data.data_source.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.slavamasa.feature_main.domain.models.Album

@Dao
interface AlbumDao {

    @Query("SELECT * FROM Album")
    fun getAllAlbums(): List<Album>

    @Query("SELECT * FROM Album WHERE albumName LIKE :name")
    fun getAlbumsByName(name: String): List<Album>

    @Query("SELECT * FROM Album WHERE albumId = :albumId")
    fun getAlbumById(albumId: Int): Album

    @Insert(onConflict = REPLACE)
    fun insertAllAlbum(albums: List<Album>)
}