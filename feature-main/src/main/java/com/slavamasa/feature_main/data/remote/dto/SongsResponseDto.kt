package com.slavamasa.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName

data class SongsResponseDto(

    @SerializedName("resultCount")
    val resultCount: Int,

    @SerializedName("results")
    val songs: List<SongDto>
)