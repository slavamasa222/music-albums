package com.slavamasa.feature_main.data.repository

import com.slavamasa.feature_main.data.data_source.daos.AlbumDao
import com.slavamasa.feature_main.data.data_source.daos.SongDao
import com.slavamasa.feature_main.data.remote.TunesApi
import com.slavamasa.feature_main.data.remote.dto.AlbumsResponseDto
import com.slavamasa.feature_main.data.remote.dto.SongsResponseDto
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.models.Song
import com.slavamasa.feature_main.domain.repository.Repository

class RepositoryImpl(
    private val tunesApi: TunesApi,
    private val albumDao: AlbumDao,
    private val songDao: SongDao
) : Repository {

    override suspend fun getRemoteAlbums(): AlbumsResponseDto {
        return tunesApi.getAlbums()
    }

    override suspend fun getRemoteSongsByAlbumId(albumId: String): SongsResponseDto {
        return tunesApi.getSongsByAlbumId(albumId)
    }

    override suspend fun getAllAlbums(): List<Album> {
        return albumDao.getAllAlbums()
    }

    override suspend fun getAlbumsByName(name: String): List<Album> {
        return albumDao.getAlbumsByName(name)
    }

    override suspend fun insertAlbum(albums: List<Album>) {
        return albumDao.insertAllAlbum(albums)
    }

    override suspend fun getAlbumById(albumId: Int): Album {
        return albumDao.getAlbumById(albumId)
    }

    override suspend fun insertAllSongs(songs: List<Song>) {
        songDao.insertAllSongs(songs)
    }

    override suspend fun getAllSongsByAlbumId(albumId: Int): List<Song> {
        return songDao.getAllSongsByAlbumId(albumId)
    }


}