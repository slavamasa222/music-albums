package com.slavamasa.feature_main.data.remote

import com.slavamasa.feature_main.data.remote.dto.AlbumsResponseDto
import com.slavamasa.feature_main.data.remote.dto.SongsResponseDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface TunesApi {

    @GET("search?term=all&entity=album&limit=200")
    suspend fun getAlbums(): AlbumsResponseDto

    @GET("lookup?entity=song")
    suspend fun getSongsByAlbumId(@Query("id") albumId: String): SongsResponseDto
}