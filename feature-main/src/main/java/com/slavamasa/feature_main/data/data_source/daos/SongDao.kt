package com.slavamasa.feature_main.data.data_source.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.slavamasa.feature_main.domain.models.Song

@Dao
interface SongDao {

    @Query("SELECT * FROM Song WHERE collectionId = :albumId")
    fun getAllSongsByAlbumId(albumId: Int): List<Song>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllSongs(songs: List<Song>)
}