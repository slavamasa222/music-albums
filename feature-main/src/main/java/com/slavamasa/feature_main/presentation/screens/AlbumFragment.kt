package com.slavamasa.feature_main.presentation.screens

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.slavamasa.feature_main.R
import com.slavamasa.feature_main.databinding.FragmentAlbumBinding
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.models.Song
import com.slavamasa.feature_main.presentation.adapters.SongsRecyclerViewAdapter
import com.slavamasa.feature_main.presentation.viewmodels.AlbumViewModel
import com.slavamasa.feature_main.presentation.viewmodels.State
import org.koin.androidx.viewmodel.ext.android.viewModel

class AlbumFragment : Fragment() {

    private lateinit var binding: FragmentAlbumBinding
    private val navArgs: AlbumFragmentArgs by navArgs()

    private val albumViewModel by viewModel<AlbumViewModel>()

    private lateinit var songsRecyclerViewAdapter: SongsRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_album, container, false)

        albumViewModel.setAlbumId(navArgs.albumId)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()

        albumViewModel.songs.observe(viewLifecycleOwner, { state ->

            when (state) {
                is State.Success<*> -> {
                    Log.d("DEBUG", "${state.data}")
                    val songs = state.data as List<Song>
                    songsRecyclerViewAdapter.differ.submitList(songs.sortedBy { it.trackNumber })
                    binding.tvNoSongsInfo.visibility = View.GONE
                }
                is State.Loading -> {
                    Toast.makeText(requireContext(), "Loading", Toast.LENGTH_SHORT).show()
                }
                is State.EmptyData -> {
                    binding.tvNoSongsInfo.visibility = View.VISIBLE
                }
                is State.Error -> {
                    Toast.makeText(requireContext(), "error: ${state.message}", Toast.LENGTH_SHORT)
                        .show()
                }

            }
        })

        albumViewModel.album.observe(viewLifecycleOwner, { state ->

            when (state) {
                is State.Success<*> -> {
                    Log.d("DEBUG", "${state.data}")

                    val album: Album = state.data as Album
                    Glide.with(requireContext())
                        .load(album.artworkUrl)
                        .placeholder(R.drawable.ic_baseline_image_24)
                        .error(R.drawable.ic_baseline_broken_image_24)
                        .into(binding.ivAlbumImage)
                    binding.tvAlbumName.text = album.albumName
                    binding.tvArtistName.text = album.artistName
                    binding.tvGenre.text = album.primaryGenreName
                    binding.tvReleaseDate.text = album.releaseDate.substringBefore('T')
                }
                is State.Loading -> {

                }
                is State.EmptyData -> {

                }
                is State.Error -> {
                    Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
                }

            }

        })
    }

    private fun initRecyclerView() {
        songsRecyclerViewAdapter = SongsRecyclerViewAdapter()
        songsRecyclerViewAdapter.differ.submitList(listOf())
        binding.recyclerViewSongs.adapter = songsRecyclerViewAdapter
        binding.recyclerViewSongs.layoutManager = LinearLayoutManager(requireContext())
    }


}