package com.slavamasa.feature_main.presentation.screens

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.slavamasa.feature_main.R
import com.slavamasa.feature_main.databinding.FragmentMainBinding
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.presentation.adapters.MainRecyclerViewAdapter
import com.slavamasa.feature_main.presentation.viewmodels.MainViewModel
import com.slavamasa.feature_main.presentation.viewmodels.State
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private lateinit var mainRecyclerViewAdapter: MainRecyclerViewAdapter

    private val mainViewModel by viewModel<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()

        mainViewModel.state.observe(viewLifecycleOwner, { state ->
            Log.d("DEBUG", "OBSERVER CALL WITH STATE: $state")
            when (state) {
                is State.Success<*> -> {
                    val albums = (state.data as List<Album>).sortedBy { it.albumName }
                    if (albums != mainRecyclerViewAdapter.differ.currentList) {
                        mainRecyclerViewAdapter.differ.submitList(albums)
                    }

                    binding.tvEmptyLibraryInfo.visibility = View.GONE
                }
                is State.EmptyData -> {
                    mainRecyclerViewAdapter.differ.submitList(listOf())
                    binding.tvEmptyLibraryInfo.visibility = View.VISIBLE
                }
                is State.Loading -> {
                    Toast.makeText(requireContext(), "Loading", Toast.LENGTH_SHORT).show()
                }
                is State.Error -> {
                    Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun initRecyclerView() {
        mainRecyclerViewAdapter = MainRecyclerViewAdapter()
        mainRecyclerViewAdapter.stateRestorationPolicy =
            RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        mainRecyclerViewAdapter.differ.submitList(listOf())
        binding.recyclerViewMain.adapter = mainRecyclerViewAdapter
        binding.recyclerViewMain.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.menu_main, menu)

        val searchView = menu.findItem(R.id.item_menu_main_search).actionView as SearchView
        searchView.setOnQueryTextListener(getOnQueryTextListener())

        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun getOnQueryTextListener(): SearchView.OnQueryTextListener {
        return object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                mainViewModel.setDataWithString("%$query%")
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                mainViewModel.setDataWithString("%$newText%")
                Log.d("DEBUG", newText.toString())
                return false
            }
        }
    }

}