package com.slavamasa.feature_main.presentation.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.usecases.MusicUseCases
import kotlinx.coroutines.*
import java.lang.Exception

class MainViewModel(private val musicUseCases: MusicUseCases) : ViewModel() {

    private val query = MutableLiveData("%%")
    private val _state: MutableLiveData<State> = Transformations.switchMap(query) {
        val result: MutableLiveData<State> = MutableLiveData(State.Loading())
        viewModelScope.launch(Dispatchers.IO) {
            result.postValue(
                getAlbumsByName(it)
            )
        }
        return@switchMap result

    } as MutableLiveData<State>
    val state: LiveData<State> = _state

    init {
        fetchData()
    }

    private fun fetchData() {
        _state.value = State.Loading()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val remoteAlbums = musicUseCases.getRemoteAlbums()
                remoteAlbums.forEach { album ->
                    insertSongsIntoDatabase(album)
                }
                insertAlbumsIntoDatabase(remoteAlbums)

            } catch (e: Exception) {
                Log.d("DEBUG", "INSIDE TRY ${e.message}")
                withContext(Dispatchers.Main) {
                    _state.value = (State.Error("Can't access server"))
                }
            }

            try {
                val databaseAlbums = musicUseCases.getAllAlbums()
                _state.postValue(
                    if (databaseAlbums.isEmpty()) {
                        State.EmptyData()
                    } else {
                        State.Success(databaseAlbums)
                    }
                )
            } catch (e: Exception) {
                _state.postValue(State.Error("Can't access local database"))
            }
        }
    }

    private suspend fun getAlbumsByName(name: String): State {
        return try {
            val albums = musicUseCases.getAlbumsByName(name)
            if (albums.isEmpty()) {
                State.EmptyData()
            } else {
                State.Success(albums)
            }

        } catch (e: Exception) {
            State.Error("Can't access local database")
        }
    }

    private suspend fun insertSongsIntoDatabase(album: Album) {

        val songs = musicUseCases.getRemoteSongsByAlbumId(album.albumId)
        musicUseCases.insertAllSongs(songs)

    }

    private suspend fun insertAlbumsIntoDatabase(albums: List<Album>) {
        try {
            musicUseCases.insertAllAlbums(albums)
        } catch (e: Exception) {
            _state.postValue(State.Error("Can't access local database"))
        }
    }

    fun setDataWithString(s: String) {
        query.value = s
    }
}