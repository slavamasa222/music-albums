package com.slavamasa.feature_main.presentation.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.slavamasa.feature_main.R
import com.slavamasa.feature_main.databinding.ItemRecyclerViewSongBinding
import com.slavamasa.feature_main.domain.models.Song

class SongsRecyclerViewAdapter : RecyclerView.Adapter<SongsRecyclerViewAdapter.ViewHolder>() {


    private val differCallback = object : DiffUtil.ItemCallback<Song>() {
        override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, differCallback)

    inner class ViewHolder(
        private val binding: ItemRecyclerViewSongBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            Log.d("DEBUG", "${differ.currentList[adapterPosition]}")
            differ.currentList[adapterPosition].let {
                binding.tvSongName.text = it.trackName
                binding.tvSongNumber.text = it.trackNumber.toString()
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRecyclerViewSongBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = differ.currentList.size
}