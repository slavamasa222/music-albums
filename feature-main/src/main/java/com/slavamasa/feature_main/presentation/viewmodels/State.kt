package com.slavamasa.feature_main.presentation.viewmodels

import com.slavamasa.feature_main.domain.models.Album

sealed class State {
    class Loading : State()
    class Success<T>(val data: T): State()
    class EmptyData: State()
    class Error(val message: String) : State()
}