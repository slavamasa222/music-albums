package com.slavamasa.feature_main.presentation.viewmodels

import androidx.lifecycle.*
import com.slavamasa.feature_main.domain.usecases.MusicUseCases
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException

class AlbumViewModel(private val musicUseCases: MusicUseCases) : ViewModel() {

    private val _album = MutableLiveData<State>(State.Loading())
    private val _songs = MutableLiveData<State>(State.Loading())
    val album: LiveData<State> = _album
    val songs: LiveData<State> = _songs

    fun setAlbumId(albumId: Int) {
        _album.value = State.Loading()
        _songs.value = State.Loading()

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val album = musicUseCases.getAlbumById(albumId)
                _album.postValue(State.Success(album))
                val songs = musicUseCases.getAllSongsByAlbumId(albumId)
                _songs.postValue(
                    if (songs.isEmpty()) {
                        State.EmptyData()
                    } else {
                        State.Success(songs)
                    }
                )

            } catch (e: Exception) {
                _songs.postValue(State.Error("Can't access local database"))
            }



        }
    }
}