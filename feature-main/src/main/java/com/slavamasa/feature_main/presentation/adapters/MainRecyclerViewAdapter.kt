package com.slavamasa.feature_main.presentation.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.slavamasa.feature_main.R
import com.slavamasa.feature_main.databinding.ItemRecyclerViewMainBinding
import com.slavamasa.feature_main.databinding.ItemRecyclerViewSongBinding
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.presentation.screens.MainFragmentDirections

class MainRecyclerViewAdapter : RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder>() {

    private val differCallback = object: DiffUtil.ItemCallback<Album>() {
        override fun areItemsTheSame(oldItem: Album, newItem: Album): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Album, newItem: Album): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this, differCallback)

    inner class ViewHolder(
        private val binding: ItemRecyclerViewMainBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener {
                Log.d("DEBUG", "${differ.currentList[adapterPosition]}")
                val action = MainFragmentDirections.actionMainFragmentToAlbumFragment(
                    differ.currentList[adapterPosition].albumId,
                    differ.currentList[adapterPosition].albumName
                )
                it.findNavController().navigate(action)
            }
        }

        fun bind() {
            differ.currentList[adapterPosition].let {
                binding.tvAlbumName.text = it.albumName
                binding.tvArtistName.text = it.artistName
                loadImage(binding.ivAlbumImage, it.artworkUrl)
            }
        }

        private fun loadImage(image: ImageView, url: String?) {
            Glide.with(image.context)
                .load(url)
                .placeholder(R.drawable.ic_baseline_image_24)
                .error(R.drawable.ic_baseline_broken_image_24)
                .into(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRecyclerViewMainBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = differ.currentList.size
}