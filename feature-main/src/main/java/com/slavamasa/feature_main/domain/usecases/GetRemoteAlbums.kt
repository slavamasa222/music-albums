package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.data.remote.dto.toAlbum
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.repository.Repository

class GetRemoteAlbums(private val repository: Repository) {

    suspend operator fun invoke(): List<Album> {
        return repository.getRemoteAlbums().albums.map { it.toAlbum() }.sortedBy { it.albumName }
    }
}