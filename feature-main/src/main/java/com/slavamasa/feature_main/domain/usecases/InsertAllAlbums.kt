package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.repository.Repository

class InsertAllAlbums(private val repository: Repository) {

    suspend operator fun invoke(albums: List<Album>) {
        repository.insertAlbum(albums)
    }
}