package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.domain.models.Song
import com.slavamasa.feature_main.domain.repository.Repository

class InsertAllSongs(private val repository: Repository) {

    suspend operator fun invoke(songs: List<Song>) {
        repository.insertAllSongs(songs)
    }
}