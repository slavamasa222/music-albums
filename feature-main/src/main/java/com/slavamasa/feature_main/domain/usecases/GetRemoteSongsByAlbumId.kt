package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.data.remote.dto.toSong
import com.slavamasa.feature_main.domain.models.Song
import com.slavamasa.feature_main.domain.repository.Repository

class GetRemoteSongsByAlbumId(private val repository: Repository) {

    suspend operator fun invoke(albumId: Int): List<Song> {
        return repository.getRemoteSongsByAlbumId(albumId.toString()).songs
            .map { it.toSong() }
            .sortedBy { it.trackNumber }
    }
}