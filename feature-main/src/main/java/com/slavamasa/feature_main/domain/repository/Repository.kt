package com.slavamasa.feature_main.domain.repository

import com.slavamasa.feature_main.data.remote.dto.AlbumsResponseDto
import com.slavamasa.feature_main.data.remote.dto.SongsResponseDto
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.models.Song

interface Repository {

    suspend fun getRemoteAlbums(): AlbumsResponseDto

    suspend fun getRemoteSongsByAlbumId(albumId: String): SongsResponseDto

    suspend fun getAllAlbums(): List<Album>

    suspend fun getAlbumsByName(name: String): List<Album>

    suspend fun insertAlbum(albums: List<Album>)

    suspend fun getAlbumById(albumId: Int): Album

    suspend fun insertAllSongs(songs: List<Song>)

    suspend fun getAllSongsByAlbumId(albumId: Int): List<Song>
}