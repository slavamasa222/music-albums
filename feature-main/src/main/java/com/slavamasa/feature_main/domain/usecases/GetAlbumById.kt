package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.repository.Repository

class GetAlbumById(private val repository: Repository) {

    suspend operator fun invoke(albumId: Int): Album {
        return repository.getAlbumById(albumId)
    }
}