package com.slavamasa.feature_main.domain.usecases

import com.slavamasa.feature_main.domain.models.Song
import com.slavamasa.feature_main.domain.repository.Repository

class GetAllSongsByAlbumId(private val repository: Repository) {

    suspend operator fun invoke(albumId: Int): List<Song> {
        return repository.getAllSongsByAlbumId(albumId)
    }
}