package com.slavamasa.feature_main.domain.usecases

data class MusicUseCases(
    val getRemoteAlbums: GetRemoteAlbums,
    val getRemoteSongsByAlbumId: GetRemoteSongsByAlbumId,
    val getAlbumsByName: GetAlbumsByName,
    val getAllAlbums: GetAllAlbums,
    val insertAllAlbums: InsertAllAlbums,
    val getAlbumById: GetAlbumById,
    val insertAllSongs: InsertAllSongs,
    val getAllSongsByAlbumId: GetAllSongsByAlbumId
)