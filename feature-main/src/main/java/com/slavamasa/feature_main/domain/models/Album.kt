package com.slavamasa.feature_main.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Album(
    @PrimaryKey
    val albumId: Int,
    val albumName: String,
    val artistName: String,
    val artworkUrl: String,
    val country: String,
    val primaryGenreName: String,
    val releaseDate: String
)
