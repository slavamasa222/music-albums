package com.slavamasa.feature_main.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Song(
    @PrimaryKey
    val trackId: Int,
    val collectionId: Int?,
    val trackName: String?,
    val trackNumber: Int?
)
