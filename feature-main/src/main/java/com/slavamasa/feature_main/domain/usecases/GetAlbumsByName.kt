package com.slavamasa.feature_main.domain.usecases

import androidx.lifecycle.LiveData
import com.slavamasa.feature_main.domain.models.Album
import com.slavamasa.feature_main.domain.repository.Repository

class GetAlbumsByName(private val repository: Repository) {

    suspend operator fun invoke(albumName: String): List<Album> {
        return repository.getAlbumsByName(albumName)
    }
}